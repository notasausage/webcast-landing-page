# GitLab Webcast Landing Page

## What is this?

A simple landing page promoting an upcoming GitLab webcast, with a sign up form where visitors can receive reminders of the webcast and get follow-up emails after the webcast has finished.

## Why did you make this?

For a quick evaluation project as part of the GitLab hiring process.

## How did you make this?

Development done with plain old HTML5 & CSS3, client-side form validation using HTML5's [constraint validation API](https://html.spec.whatwg.org/multipage/form-control-infrastructure.html#the-constraint-validation-api) & vanilla JavaScript (with the help of dependency-free [validate.js](http://rickharrison.github.io/validate.js/)), and design in [Sketch](https://sketchapp.com).

## Why didn't you use a CSS preprocessor/templating system/static site generator/JavaScript library?

I certainly have used all of those things before. [Sass](https://sass-lang.com), [Haml](http://haml.info), [Handlebars](http://handlebarsjs.com), and [Gulp](https://gulpjs.com) are all part of my usual workflow. But after considering the simplicity of this project, and reading Frank Chimero's recently posted [Everything Easy is Hard Again](https://frankchimero.com/writing/everything-easy-is-hard-again/), I decided to keep it simple and build a website without all the razzle dazzle. And since I wanted to complete this project in just a few days, the setup of all of those extraneous "helpers" seemed like a waste of time.

## Where can I see it?

It's currently live via GitLab Pages at http://notasausage.gitlab.io/webcast-landing-page/

![Webcast Landing Page mockups](http://hanerino.com/images/gitlab-webcast-landing-page.png)
